#include<stdio.h>
#include<math.h>
# define PI 3.14
float get_radius()
{
  float radius;
  printf("enter radius\n");
  scanf("%f",&radius);
  return radius;
}
float compute_area(float radius)
{
  float area;
  area=PI*radius*radius;
  return area;
}
float compute_circumf(float radius)
{
  float C;
  C=2*PI*radius;
  return C;
}
float output(float radius,float area,float C)
{   
  printf("the area and circumference of the circle with radius %f is %f and %f\n",radius,area,C);
}
int main()
{  
  float radius,area,cir;
  radius=get_radius();
  area=compute_area(radius);
  cir=compute_circumf(radius);
output(radius,area,cir);
  return 0;
}
  